from flask import Flask, request
from flask_restful import Resource, Api
import pandas as pd
import json
from bs4 import BeautifulSoup
from flask_cors import CORS

def fix(comment):
    try:
        return BeautifulSoup(comment, "lxml").string
    except:
        pass

df = pd.read_csv('../scrubbed.csv')
df[['month', 'day', 'year']] = df['datetime'].str.split('/', expand=True)
df = df.drop('datetime', axis=1)
df[['year_t', 'time']] = df['year'].str.split(' ', expand=True)
df[['month', 'day', 'year']] = df[['month', 'day', 'year_t']].apply(pd.to_numeric)
#df['year'] = df['year_t'].apply(pd.to_numeric)
df = df.drop('year_t', axis=1)
df['comments_fixed'] = [fix(comment) for comment in df['comments']]
df = df.drop(['comments'], axis=1)
df = df.rename(index=str, columns={'comments_fixed': 'comments'})

app = Flask(__name__)
api = Api(app)
CORS(app)

class GetSince(Resource):
    def since(self, year, count):
        data = df.loc[df['year'] >= int(year)]
        data = data.head(count)
        result = data.to_json(orient='records')
        return result

    def get(self, year, count):
        result = self.since(year, count)
        result = json.dumps(result)
        return result

class GetBetween(Resource):
    def between(self, year1, year2, count):
        years = []
        t = year1
        while t != year2 + 1:
            years.append(t)
            t += 1

        data = df.loc[df['year'].isin(years)]
        data = data.head(count)
        result = data.to_json(orient='records')
        return result

    def get(self, year1, year2, count):
        result = self.between(year1, year2, count)
        return result

api.add_resource(GetSince, '/since/<int:year>/<int:count>')
api.add_resource(GetBetween, '/between/<int:year1>/<int:year2>/<int:count>')

@app.route('/')
def hello_world():
    return 'о динар привет как дел'

if __name__ == '__main__':
    app.run()
